(package (wak-wt-tree (0))
  (depends)

  (synopsis "Weight-balanced trees")
  (description
   "Functional implementation of weight-balanced trees, suitable for use"
   "as discrete sets or discrete maps, as well as priority queues.")

  (libraries (sls -> "wak")))

;; Local Variables:
;; scheme-indent-specs: (pkg-list)
;; End:
